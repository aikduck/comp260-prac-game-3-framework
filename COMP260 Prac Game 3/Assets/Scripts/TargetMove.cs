﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

    private Animator animator;
    public float startTime = 0.0f;
    public float animationSpeed = 1;

    void Start()
    {
        // get the animator component
        animator = GetComponent<Animator>();
        animator.speed = animationSpeed;
    }

    void Update()
    {
        // set the Start parameter to true 
        // if we have passed the start time
        animator.SetBool("Start", Time.time >= startTime);
    }

    void Destroy()
    {
        Destroy(gameObject);
    }


    void OnCollisionEnter(Collision collision)
    {
        // transition to the Die animation
        animator.SetBool("Hit", true);
    }

}
