﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour
{
    public BulletMove bulletPrefab;

    public float shotDelay = 0.3f;

    private float lastShotTime = 0;

    void Update()
    {
        // do not run if the game is paused
        if (Time.timeScale == 0)
        {
            return;
        }


        // when the button is pushed, fire a bullet
        if (Input.GetButtonDown("Fire1") && (Time.time - lastShotTime) >= shotDelay)
        {
            lastShotTime = Time.time;

            BulletMove bullet = Instantiate(bulletPrefab);
            // the bullet starts at the player's position
            bullet.transform.position = transform.position;

            // create a ray towards the mouse location
            Ray ray =
                Camera.main.ScreenPointToRay(Input.mousePosition);
            bullet.direction = ray.direction;
        }
    }

}
